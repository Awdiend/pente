#pragma once
#include <stdexcept>
#include <chrono>

/// Use start and stop fucntions to time an event
/// keep track of the mean time of events and total time spent recording
/// used [cxxtimer](https://github.com/andremaravilha/cxxtimer) as an inspiration
class Timer
{
private:
    bool isStarted;
    unsigned int nbTimings;
    std::chrono::steady_clock::time_point _timeStart;
    std::chrono::duration<long double> _totalTime;

public:

    Timer(): isStarted(false), nbTimings(0), _timeStart(std::chrono::steady_clock::now()), _totalTime(std::chrono::duration<long double>(0))
    { }

    /// starts a timing
    void start()
    {
        isStarted = true;
        _timeStart = std::chrono::steady_clock::now();
    }

    /// stops a timing, returns the time elapsed
    template<typename Precision>
    Precision stop()
    {
        // computes the elapsed time
        std::chrono::steady_clock::time_point timeStop = std::chrono::steady_clock::now();
        if(not isStarted) throw std::logic_error("You stopped a timer that you did not start !");
        std::chrono::duration<long double> elapsedTime = std::chrono::duration_cast<std::chrono::duration<long double>>(timeStop - _timeStart);
        // updates the inner counters
        isStarted = false;
        _totalTime += elapsedTime;
        nbTimings++;
        // returns the elapsed time
        return std::chrono::duration_cast<Precision>(elapsedTime);
    }

    /// total time elapsed since the beginning
    template<typename Precision>
    Precision totalTime() const
    {
        return std::chrono::duration_cast<Precision>(_totalTime);
    }

    /// mean time elapsed per timing
    template<typename Precision>
    Precision meanTime() const
    {
        return std::chrono::duration_cast<Precision>(_totalTime / nbTimings);
    }
};
